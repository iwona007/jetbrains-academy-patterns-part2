# jetBrains-Academy-patterns-part2

#decorator
This fun task involves Pizza decorating. Imagine that the basic ingredients are used, but you want to add something more.
Sample Output 1:
Vegan $4.99
Vegan, Broccoli, Tomato, Spinach $5.27
MeatHeaven, Ham, Chicken, Cheese $6.70


#decoratorBreakfast
They say that breakfast is the most important meal of the day: if that's true, it better be fancy. In this task,
we offer you to decorate a slice of bread for your perfect developer's morning. Do not forget about calories!
Sample Input 1:

Sample Output 1:
Bagel, Butter, Butter, Ham, Cheese. kCal: 2890
Bun, Butter, Jam, Jam. kCal: 440

#DecoratorCoffee
Now your task is to make some Coffee. Your Decorators are different fillers; as a pro barista, you are provided with 
Sugar, Milk and Whip.

Sample Output 1:
Espresso $1.99
Espresso, Milk, Sugar $2.14
Instant Coffee, Whip, Sugar, Sugar, Sugar $1.16
I'm drinking my Instant Coffee, Whip, Sugar, Sugar, Sugar
-I want to add some Whip to my coffee. And don't make a new one! Just add Whip
-Okay! But the final price will change
-I understand
Instant Coffee, Whip, Sugar, Sugar, Sugar, Whip $1.26

