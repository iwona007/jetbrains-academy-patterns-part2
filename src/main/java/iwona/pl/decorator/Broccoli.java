package iwona.pl.decorator;

public class Broccoli extends Decorator {

    private Pizza pizza;

    public Broccoli(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + ", broccoli";
    }

    @Override
    double cost() {
        return .10 + pizza.cost();
    }
}
