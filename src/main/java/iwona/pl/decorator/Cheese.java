package iwona.pl.decorator;

public class Cheese extends Decorator {
    private Pizza pizza;

    public Cheese(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + ", cheese";
    }

    @Override
    double cost() {
        return .20 + pizza.cost();
    }
}
