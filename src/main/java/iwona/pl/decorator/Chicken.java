package iwona.pl.decorator;

public class Chicken extends Decorator {
    private Pizza pizza;

    public Chicken(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + ", Chicken";
    }

    @Override
    double cost() {
        return pizza.cost() + 1.5;
    }
}
