package iwona.pl.decorator;

public abstract class Decorator extends Pizza{

    public abstract String getDescription();
}
