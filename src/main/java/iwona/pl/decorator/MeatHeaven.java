package iwona.pl.decorator;

public class MeatHeaven extends Pizza {
    public MeatHeaven() {
        description = "MeatHeaven";
    }

    @Override
    double cost() {
        return 4.0;
    }
}
