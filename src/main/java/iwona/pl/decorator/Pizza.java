package iwona.pl.decorator;

public abstract class Pizza {
    String description;

    public String getDescription(){
        return description;
    }

    abstract double cost();
}
