package iwona.pl.decorator;

import java.util.Locale;

public class PizzaMain {
    public static void main(String[] args) {

        Pizza vegaPizza = new Vegan();
        System.out.println(vegaPizza.getDescription() + " $" + formatSum(vegaPizza.cost()));

        Pizza veganDecorator = new Vegan();
       veganDecorator = new Broccoli(veganDecorator);
       veganDecorator = new Tomato(veganDecorator);
       veganDecorator = new Spinach(veganDecorator);
        System.out.println(veganDecorator.getDescription() + " $" + formatSum(veganDecorator.cost()));

        Pizza meatPizzaDecor = new MeatHeaven();
        meatPizzaDecor = new Ham(meatPizzaDecor);
        meatPizzaDecor = new Chicken(meatPizzaDecor);
        meatPizzaDecor = new Cheese(meatPizzaDecor);
        System.out.println(meatPizzaDecor.getDescription() + " $" + formatSum(meatPizzaDecor.cost()));
    }

    private static String formatSum(double sum){
        return String.format(Locale.ROOT, "%.2f", sum);
    }
}
