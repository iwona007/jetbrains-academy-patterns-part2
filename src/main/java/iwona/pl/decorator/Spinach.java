package iwona.pl.decorator;

public class Spinach extends Decorator{

    private Pizza pizza;

    public Spinach(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + ", spinach";
    }

    @Override
    double cost() {
        return .09 + pizza.cost();
    }
}
