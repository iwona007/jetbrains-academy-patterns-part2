package iwona.pl.decorator;

public class Tomato extends Decorator {

    private Pizza pizza;

    public Tomato(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + ", tomato";
    }

    @Override
    double cost() {
        return .09 + pizza.cost();
    }
}
