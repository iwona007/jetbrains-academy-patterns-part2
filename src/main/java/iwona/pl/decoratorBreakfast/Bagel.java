package iwona.pl.decoratorBreakfast;

public class Bagel extends Bread {

    public Bagel() {
        description ="Bagiel";
    }

    @Override
    int getKcal() {
        return 250;
    }
}
