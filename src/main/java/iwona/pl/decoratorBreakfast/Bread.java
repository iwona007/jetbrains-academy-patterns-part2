package iwona.pl.decoratorBreakfast;

abstract class Bread {
    String description;
    int kcal;

 String getDescription() {
        return description;
    }

   abstract int getKcal();

    @Override
    public String toString() {
        return getDescription() + ". kCal: " + getKcal();
    }
}
