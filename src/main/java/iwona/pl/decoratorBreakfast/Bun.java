package iwona.pl.decoratorBreakfast;

public class Bun extends Bread {

    Bun() {
        description = "Bun";
    }

    @Override
    int getKcal() {
        return 150;
    }
}
