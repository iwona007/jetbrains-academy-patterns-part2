package iwona.pl.decoratorBreakfast;

public class Cheese extends Decorator {

    private Bread bread;

    public Cheese(Bread bread) {
        this.bread = bread;
    }

    @Override
    String getDescription() {
        return bread.getDescription() + ", Cheese";
    }

    @Override
    int getKcal() {
        return bread.getKcal() +40;
    }
}
