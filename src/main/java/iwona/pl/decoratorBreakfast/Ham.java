package iwona.pl.decoratorBreakfast;

public class Ham extends Decorator {

    private Bread bread;

    public Ham(Bread bread) {
        this.bread = bread;
    }

    @Override
    String getDescription() {
        return bread.getDescription() + ", Ham";
    }

    @Override
    int getKcal() {
        return bread.getKcal() + 2500;
    }
}
