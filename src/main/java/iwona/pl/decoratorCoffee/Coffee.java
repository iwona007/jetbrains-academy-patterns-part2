package iwona.pl.decoratorCoffee;

public abstract class Coffee {

   public String description;

    public String getDescription() {
        return description;
    }

    abstract double cost();

    @Override
    public String toString() {
        return  getDescription();

    }
}
