package iwona.pl.decoratorCoffee;

public abstract class Decorator extends Coffee{

    public abstract String getDescription();

}
