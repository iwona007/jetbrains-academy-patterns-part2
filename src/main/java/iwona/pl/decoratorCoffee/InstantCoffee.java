package iwona.pl.decoratorCoffee;

public class InstantCoffee extends Coffee {

    public InstantCoffee() {
        description = "InstantCoffee";
    }

    @Override
    double cost() {
        return 1.0;
    }
}
