package iwona.pl.decoratorCoffee;

public class Milk extends Decorator {

    private Coffee coffee;

    public Milk(Coffee coffee){
        this.coffee = coffee;
    }

    @Override
    public String getDescription() {
        return coffee.getDescription() + ", Milk";
    }

    @Override
    double cost() {
        return .13 + coffee.cost();
    }
}
