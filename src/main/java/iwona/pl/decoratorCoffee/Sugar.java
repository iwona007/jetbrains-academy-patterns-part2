package iwona.pl.decoratorCoffee;

public class Sugar extends Decorator {

    private Coffee coffee;

    public Sugar(Coffee coffee){
        this.coffee = coffee;
    }

    @Override
    public String getDescription() {
        return coffee.getDescription() + ", sugar";
    }

    @Override
    double cost() {
        return .02 + coffee.cost();
    }
}
