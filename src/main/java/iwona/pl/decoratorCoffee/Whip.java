package iwona.pl.decoratorCoffee;

public class Whip extends Decorator {

    private Coffee coffee;

    public Whip(Coffee coffee){
        this.coffee = coffee;
    }
    @Override
    public String getDescription() {
        return coffee.getDescription() + ", whip";
    }

    @Override
    double cost() {
        return .10 + coffee.cost();
    }
}
